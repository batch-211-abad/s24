/*


Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)


*/




/*

Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…

*/

let num = 3;
let getCube = `The cube of ${num} is ${num ** 3}.`;
console.log(getCube);


let address = {
	houseNumber: "258",
	streetName: "Washington Avenue",
	area: "NW",
	state: "California",
	postCode: "90011"

}


/*
5. Create a variable address with a value of an array containing details of an address.
6. Destructure the array and print out a message with the full address using Template Literals.
*/


let addressDisplay = `I live at ${address.houseNumber} ${address.streetName} ${address.area}, ${address.state} ${address.postCode}`
console.log(addressDisplay);

/*
// Object Destructuring
7. Create a variable animal with a value of an object data type with different animal details as it’s properties.
8. Destructure the object and print out a message with the details of the animal using Template Literals.
*/

let animal = {
	name: "Lolong",
	classification: "saltwater crocodile",
	weight: 1075,
	length_feet: 20,
	length_inch: 3,
}


let animalDisplay = `${animal.name} was a ${animal.classification}. He weighed at ${animal.weight} kgs with a measurement of ${animal.length_feet} ft ${animal.length_inch} in.`
console.log(animalDisplay);



/*
// Arrow Functions
9. Create an array of numbers.

// A
10. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.
*/

const numbers = [1, 2, 3, 4, 5];


numbers.forEach((number)=>{
	console.log(`${number}`)
});

let reduceNumber = numbers.reduce((x, y)=> x + y)
	console.log(reduceNumber);




/*

// B
11. Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.




// Javascript Objects
12. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
13. Create/instantiate a new object from the class Dog and console log the object.

*/

class Dog {
	constructor(name, age, breed){
		this.name = name,
		this.age = age,
		this.breed = breed
	}
}

const newDog = new Dog ("Frankie", 5, "Miniature Daschund");
console.log(newDog);